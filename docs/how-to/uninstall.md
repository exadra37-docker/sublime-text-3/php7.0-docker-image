# HOW TO UNINSTALL

This project is used byt the [Docker Sublime Text 3 Bash Interface](https://gitlab.com/exadra37-docker/sublime-text-3/bash-interface), therefore their uninstall instructions must be followed.


---

[<< previous](create_a_merge_request.md) | [next >>](./../../AUTHOR.md)

[HOME](./../../README.md)

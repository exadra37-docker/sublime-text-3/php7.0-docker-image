# HOW TO USE

To run this Docker image we must use the [Docker Sublime Text 3 Bash Interface](https://gitlab.com/exadra37-docker/sublime-text-3/bash-interface) and follow their instructions.


---

[<< previous](install.md) | [next >>](./../../CONTRIBUTING.md)

[HOME](./../../README.md)

# DOCKER IMAGE FOR SUBLIME TEXT 3 WITH PHP

This project aims to provide a Docker Image to run Sublime Text 3 with PHP 7 from inside a Docker Container, by using the [Docker Sublime Text 3 Bash Interface](https://gitlab.com/exadra37-docker/sublime-text-3/bash-interface).


## MENU

* **How To**
    + [Install](./docs/how-to/install.md)
    + [Use](./docs/how-to/use.md)
    + [Report an Issue](./docs/how-to/create_an_issue.md)
    + [Create a Branch](./docs/how-to/create_branches.md)
    + [Open a Merge Request](./docs/how-to/create_a_merge_request.md)
    + [Uninstall](./docs/how-to/uninstall.md)
* **Road Map**
    + [Milestones](https://gitlab.com/exadra37-docker/sublime-text-3/php-docker-image/milestones)
    + [Overview](https://gitlab.com/exadra37-docker/sublime-text-3/php-docker-image/boards)
* **About**
    + [Author](AUTHOR.md)
    + [Contributors](CONTRIBUTORS.md)
    + [Contributing](CONTRIBUTING.md)
    + [Project Description](DESCRIPTION.md)
    + [License](LICENSE)


## SUPPORT DEVELOPMENT

If this is useful for you, please:

* Share it on [Twitter](https://twitter.com/home?status=https%3A//github.com/exadra37-versioning/explicit-versioning%20a%20%23versioning%20specification%20for%20%23developers%20that%20care%20about%20release%20%23software%20with%20explicit%20breaking%20changes.%23semver) or in any other channel of your preference.
* Consider to [offer me](https://www.paypal.me/exadra37) a coffee, a beer, a dinner or any other treat 😎.


## EXPLICIT VERSIONING

This repository uses [Explicit Versioning](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## BRANCHES

Branches are created as demonstrated [here](docs/how-to/create_branches.md).

This are the type of branches we can see at any moment in the repository:

* `master` - issues and milestones branches will be merge here.
* `last-stable-release` - matches the last stable tag created. Useful for automation tools.
* `issue-4_fix-email-validation` (issue-number_title) - each issue will have is own branch for development.
* `milestone-12_add-cache` (milestone-number_title) - all Milestone issues will start, track and merged here.

Only `master` and `last-stable-release` branches will be permanent ones in the repository and all other ones will be
removed once they are merged.

# PROJECT DESCRIPTION

This project builds the Docker Image to be used by [Docker Sublime Text 3 Bash Interface](https://gitlab.com/exadra37-docker/sublime-text-3/bash-interface) when selecting the PHP profile to run Sublime Text 3 from inside a Docker Container for PHP Development.

This Docker Image provides:
	
	* Sublime Text 3
	* PHP 7
	* Xdebug
	* Git


---

[<< previous](./CONTRIBUTORS.md) | [next >>](LICENSE)

[HOME](./../../README.md)
